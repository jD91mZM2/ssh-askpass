# Abandoned

This is sort of abandoned, I decided to stick with the GPG agent as I could apparently make my SSH key be a subkey of my GPG key, and in that way have everything in one place.
I therefore have no need to maintain this anymore, when the gpg-agent has something like that built in.
Of course, anyone is free to fork this project and maintain it, and/or ping me and tell my why this should be maintained. That said, it's pretty stable so it probably won't need much maintaining...

# pinentry as $SSH_ASKPASS

I'm fully aware of enable-ssh-support in GPG, but it's been a pain to
debug at times when I forgot you need to run `ssh-add` once first
before it'll start understanding your key. I've been thinking of
making stuff simpler by using funtoo's `keychain` program, which could
be great if it wasn't for the lack of pinentry...

This is a simple python 3 script without dependencies you can assign
SSH_ASKPASS to in order to use pinentry without involving GPG.
