#!/usr/bin/env python

import subprocess
import sys
import urllib.parse

# Run the subprocess
data = b"""
setdesc Enter the passphrase to your encrypted SSH key.
setprompt Passphrase:
getpin
bye
"""

process = subprocess.run(
    ["pinentry"] + sys.argv[1:],
    input=data,
    stdout=subprocess.PIPE,
)

# Search for "D " in the output
lines = process.stdout.splitlines()

try:
    passphrase = next(filter(
        lambda line: line.startswith(b"D "),
        reversed(lines)
    ))[2:]
except StopIteration:
    try:
        # Nope, print error and exit
        error = next(filter(
            lambda line: line.startswith(b"ERR "),
            reversed(lines)
        ))
        sys.stdout.buffer.write(error)
        sys.stdout.buffer.write(b"\n")
    except StopIteration:
        sys.stdout.buffer.write(process.stdout)
    sys.exit(1)

# Decode the URL-encoded passphrase
passphrase = urllib.parse.unquote_to_bytes(passphrase)

# Print out the final passphrase
sys.stdout.buffer.write(passphrase)
sys.stdout.buffer.write(b"\n")
