{
  pkgs ? import <nixpkgs> {},
  stdenv      ? pkgs.stdenv,
  lib         ? pkgs.lib,
  python37    ? pkgs.python37,
  pinentry    ? pkgs.pinentry,
  makeWrapper ? pkgs.makeWrapper,

  # .override-able options

  # Whether or not pinentry should grab the keyboard globally to prevent keyloggers
  globalGrab ? true,
  # Extra options to pinentry
  extraFlags ? [],
}:

let
  flags = extraFlags
          ++ (if globalGrab then [] else [ "--no-global-grab" ]);
  addFlags = lib.concatMap (flag: ["--add-flags" flag]) flags;
in
stdenv.mkDerivation rec {
  pname = "ssh-askpass";
  version = "git";

  src = ./.;

  buildInputs = [ python37 ];
  nativeBuildInputs = [ makeWrapper ];

  installPhase = ''
    install -Dm 644 LICENSE $out/LICENCE
    install -Dm 644 README.md $out/README.md
    install -Dm 755 ssh-askpass.py $out/bin/ssh-askpass.py
    wrapProgram $out/bin/ssh-askpass.py \
      --prefix PATH : "${lib.makeBinPath [ pinentry ]}" \
      ${lib.escapeShellArgs addFlags}
  '';

  meta = {
    description = "pinentry as $SSH_ASKPASS";
    license = stdenv.lib.licenses.mit;
  };
}
