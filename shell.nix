{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
  # Things to be put in $PATH
  nativeBuildInputs = with pkgs; [ python37 ];

  # Libraries to be installed
  buildInputs = with pkgs; [ pinentry ];
}
